package com.higofiturindonesia.higo;

import org.json.JSONObject;

import com.higofiturindonesia.higo.page.ChooseCategoryPage;
import com.higofiturindonesia.higo.util.Constant;
import com.higofiturindonesia.higo.util.PushServer;
import com.higofiturindonesia.higo.util.SharedPref;
import com.higofiturindonesia.higo.util.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ToggleButton;

public class SignUpActivity extends Activity {
	
	Button signup;
	ImageView back;
	ToggleButton btnSwitch;
	EditText inputFullname, inputEmail, inputPassword;
	ProgressBar progress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_signup);
		
		Utils.setPolicyThread();

		back = (ImageView) findViewById(R.id.back);
		signup = (Button) findViewById(R.id.signup);

		inputFullname = (EditText) findViewById(R.id.fullname);
		inputEmail = (EditText) findViewById(R.id.email);
		inputPassword = (EditText) findViewById(R.id.password);
		
		btnSwitch = (ToggleButton) findViewById(R.id.toggle);

		progress = (ProgressBar) findViewById(R.id.progress);
		progress.setVisibility(View.GONE);

		signup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// startActivity(new Intent(LoginPage.this,
				// MainActivity.class));
				if (checkProses())
					showDialogProcess();
			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
	}
	
	boolean checkProses() {
		
		String em = inputEmail.getText().toString().trim();
		String pwd = inputPassword.getText().toString().trim();
		String fullName = inputFullname.getText().toString().trim();
		
		if (fullName.length()<1) {
			Utils.toast(this, "Fullname invalid...");
			return false;
		}
		
		if (em.length()<1) {
			Utils.toast(this, "Email invalid...");
			return false;
		}
		
		if (!Utils.isValidEmail(em)) {
			Utils.toast(this, "Email invalid...");
			return false;
		}
		
		if (pwd.length()<1) {
			Utils.toast(this, "Password invalid...");
			return false;
		}
		
		if (!btnSwitch.isChecked()) {
			Utils.toast(this, "Terms of Service should be checked...");
			return false;
		}
		
		return true;
	}
	
	// var url = "\(Constant.getAPIMember())&action=insert_member_step1&em=\(em)&pass=\(ps)&fnm=\(fnm)&mei\(imei)"
    
	AlertDialog alertDialog;

	public void showDialogProcess() {

		// check tanggal berangkat dan tanggal pulang

		View vg = this.getLayoutInflater().inflate(R.layout.dialog_progress,
				null);

		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setView(vg);
		alertDialog.setCancelable(true);
		alertDialog.show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (processSignup != null)
					processSignup.cancel(true);

				processSignup = new ProcessSignup();
				processSignup.execute();

				// alertDialog.cancel();
			}
		}, 900);

	}

	ProcessSignup processSignup;

	public class ProcessSignup extends AsyncTask<Void, Void, Void> {

		boolean check = false;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			if (!alertDialog.isShowing())
				alertDialog.show();

			check = false;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			SystemClock.sleep(1000 * 2);

			// http://higoapps.com/api/member_api.php?action=log_in&em=rully.hasibuan@gmail.com&pass=123456

			String em = inputEmail.getText().toString().trim();
			String pwd = inputPassword.getText().toString().trim();
			String fullName = inputFullname.getText().toString().trim();

			String uri = Constant.URI_BASE_API
					+ "member_api.php?&action=insert_member_step1";
			String data = "&em="+em + "&pass="+pwd+"&fnm="+fullName
					+ "&mei="+Utils.getIMEITelephone(SignUpActivity.this);

			Utils.log("uri: " + uri);
			Utils.log("data: " + data);

			try {
				String resp = PushServer.getResponsePush(uri, data);
				JSONObject json = new JSONObject(resp);
				if ("200".equals(json.getString("code"))) {
					check = true;
					
					JSONObject jData = json.getJSONArray("result").getJSONObject(0);
					Utils.log("Data response login; "+jData.toString());
					
					SharedPref.saveResponseUserLoginPref(SignUpActivity.this, jData.toString());
					SharedPref.saveStatusPref(SignUpActivity.this, "1");
				}
				else {
					SharedPref.saveResponseUserLoginPref(SignUpActivity.this, "");
					SharedPref.saveStatusPref(SignUpActivity.this, "0");
				}
				
			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (alertDialog.isShowing())
				alertDialog.cancel();
			
			if (check) {
				Utils.toast(SignUpActivity.this, "Sign up success");
				Intent i = new Intent(SignUpActivity.this, ChooseCategoryPage.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
				
				setResult(Activity.RESULT_OK);
				finish();
			} else {
				Utils.toast(SignUpActivity.this, "Sign up failed...");
			}

		}

	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
