package com.higofiturindonesia.higo.page;

import com.higofiturindonesia.higo.R;
import com.higofiturindonesia.higo.extend.CircleImageView;
import com.higofiturindonesia.higo.model.UserModel;
import com.higofiturindonesia.higo.util.ImageLoader;
import com.higofiturindonesia.higo.util.Utils;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfilePage extends Activity {

	TextView nameProfile, textProfile, textNotification, textAboutUs,
			textLegal, textSignout;
	ImageView btnBack;
	CircleImageView imageProfile;
	UserModel member = new UserModel();

	ImageLoader imgLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile);

		Utils.setPolicyThread();
		member = UserModel.UserModelFromJson(this);

		nameProfile = (TextView) findViewById(R.id.nameProfile);
		textProfile = (TextView) findViewById(R.id.textProfile);
		textNotification = (TextView) findViewById(R.id.textNotification);
		textAboutUs = (TextView) findViewById(R.id.textAboutUs);
		textLegal = (TextView) findViewById(R.id.textLegal);
		textSignout = (TextView) findViewById(R.id.textSignout);

		btnBack = (ImageView) findViewById(R.id.btnBack);
		imageProfile = (CircleImageView) findViewById(R.id.imageProfile);

		String mName = "";
		if (member.getMiddleName().trim().length()>0)
			mName = " " + member.getMiddleName();
		
		nameProfile.setText(""+member.getFirstName()+mName+" "+member.getLastName());
		
		imgLoader = new ImageLoader(this);
		if (member.getFotoMember().length() > 5) {
			String uri = member.getFotoMember();
			imgLoader
					.DisplayImage(uri, R.drawable.profile, imageProfile);
		}

		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		textSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(Activity.RESULT_OK);
				finish();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
