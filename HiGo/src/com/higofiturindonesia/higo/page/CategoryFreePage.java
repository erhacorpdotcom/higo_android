package com.higofiturindonesia.higo.page;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.higofiturindonesia.higo.R;
import com.higofiturindonesia.higo.util.Utils;

public class CategoryFreePage extends Activity {

	RelativeLayout tabBar;
	GridLayout gridLayout;
	ImageButton categFree;
	ImageView btnBack, btnSearch;
	
	int heightScreen = 0;
	int[] getScreen;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gridlayout_free);
		
		Utils.setPolicyThread();
		
		getScreen = Utils.getScreenSize(this);
		heightScreen = getScreen[1];
		
		tabBar = (RelativeLayout) findViewById(R.id.tabBar);
		gridLayout = (GridLayout) findViewById(R.id.gridLayout);
		categFree = (ImageButton) findViewById(R.id.categFree);
		
		ViewGroup.LayoutParams layoutGrid = gridLayout.getLayoutParams();
		if (heightScreen>1024) {
			
			layoutGrid.height = (heightScreen/2) + 500;
			gridLayout.setLayoutParams(layoutGrid);
			
			layoutGrid = gridLayout.getLayoutParams();
			Utils.log("layoutGrid height222: "+layoutGrid.height);
		}
		
		int restHeight = heightScreen - tabBar.getLayoutParams().height - gridLayout.getLayoutParams().height;
		Utils.log("height screen: "+heightScreen+" restHeight: "+restHeight);
		
		int moreless = restHeight-categFree.getLayoutParams().height;
		Utils.log("moreless: "+moreless);
		
		if (moreless!=0) {
			int getCategfreeheight = 0;
			
			if (moreless < 0) {
				getCategfreeheight = 6 - moreless;
			}
			else 
				getCategfreeheight = heightScreen - moreless + 6;
			
			Utils.log("getCategfreeheight: "+getCategfreeheight);
			
			ViewGroup.LayoutParams layout = categFree.getLayoutParams();
			layout.height = getCategfreeheight;
			categFree.setLayoutParams(layout);
		}
		
		btnSearch = (ImageView) findViewById(R.id.btnSearch);
		btnSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Utils.log("btnSearch.setOnClickListener: ");

			}
		});

		btnBack = (ImageView) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		categFree.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(CategoryFreePage.this,
						ContentFreePage.class);
				startActivity(i);
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
