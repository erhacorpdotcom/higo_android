package com.higofiturindonesia.higo.page;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.higofiturindonesia.higo.MainActivity;
import com.higofiturindonesia.higo.R;
import com.higofiturindonesia.higo.adapter.GridDashboardAdapter;
import com.higofiturindonesia.higo.model.UserModel;
import com.higofiturindonesia.higo.model.VendorModel;
import com.higofiturindonesia.higo.util.Constant;
import com.higofiturindonesia.higo.util.PushServer;
import com.higofiturindonesia.higo.util.SharedPref;
import com.higofiturindonesia.higo.util.Utils;

public class ChooseVendorPage extends Activity {

	GridView gridView;
	LinearLayout layoutBottom;
	Button btnNext;
	ImageView btnBack;

	GridDashboardAdapter adapter;
	ArrayList<VendorModel> vendors = new ArrayList<VendorModel>();
	
	Bundle extras;
	String tempCateg;
	String tempMultiCateg;
	
	boolean[] checked = new boolean[] { false, false, false, false, false,
			false, false, false };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_category);

		Utils.setPolicyThread();
		member = UserModel.UserModelFromJson(this);
		
		extras = getIntent().getExtras();
		try {
			tempCateg = extras.getString("categ");
			tempMultiCateg = extras.getString("multi_categ");
			
			member.setTempCateg(tempCateg);
			member.setTempMultiCateg(tempMultiCateg);
		}
		catch(Exception e) {}
		
		layoutBottom = (LinearLayout) findViewById(R.id.layoutBottom);
		btnNext = (Button) findViewById(R.id.next);
		btnBack = (ImageView) findViewById(R.id.btnBack);
		btnBack.setVisibility(View.VISIBLE);

		gridView = (GridView) findViewById(R.id.gridView);
		gridView.setBackgroundResource(R.color.white);
		
		adapter = new GridDashboardAdapter(this, vendors, false);
		gridView.setNumColumns(4);
		gridView.setAdapter(adapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (position > -1)
					adapter.toggleChecked(position);

				checkColorNextButton();
			}
		});
		
		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				
			}
		});

		layoutBottom.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clickActionBottom();
				
			}
		});
		
		btnNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clickActionBottom();
				
			}
		});
		
		// uri get vendor by pref click category member
		// // http://higoapps.com/api/vendor_api.php?action=get_vendor_bycategory&im=5&c=7&mc=2,7
		
		// get vendor by shredPref;
		String response = SharedPref.getResponseVendorPref(this);
		if (response.length()>0) {
			reloadVendorBySharedPref();
		}
		//else {
		//	Utils.toast(this, "please wait...");
			showDialogProcessVendor();
		//}

	}
	
	void reloadVendorBySharedPref() {
		String response = SharedPref.getResponseVendorPref(this);
		if (response.length()>0) {
			try {
				JSONArray jArr = new JSONArray(response);
				Utils.log("get json array length: "+jArr.length());
				vendors = new ArrayList<VendorModel>();
				
				for (int j=0; j<jArr.length(); j++) {
					VendorModel v = VendorModel.setVendorFromModelJSON(jArr.get(j).toString());
					vendors.add(v);
				}
			}
			catch(Exception e) {}
			
			Utils.log("get vendors size: "+vendors.size());
			adapter.setItemData(vendors);
		}
	}
	
	public void showDialogProcessVendor() {

		// check tanggal berangkat dan tanggal pulang

		View vg = this.getLayoutInflater().inflate(R.layout.dialog_progress,
				null);

		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setView(vg);
		alertDialog.setCancelable(true);
		alertDialog.show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// reload syncrohinze vendors API
				if (processGetVendors!=null)
					processGetVendors.cancel(true);
				
				processGetVendors = new ProcessGetVendors();
				processGetVendors.execute();
			}
		}, 900);

	}
	
	public void clickActionBottom() {
		if (adapter.countCheckedItem() > 0) {
			preparePushDashboard();
		} else {
			Utils.toast(ChooseVendorPage.this,
					"Vendor not selected yet...");
		}
	}

	UserModel member = new UserModel();

	void preparePushDashboard() {
		if (member.getIdMember() != null) {
			Utils.log("get member id: " + member.getIdMember());
			showDialogProcess();
		}
	}

	AlertDialog alertDialog;

	public void showDialogProcess() {

		// check tanggal berangkat dan tanggal pulang

		View vg = this.getLayoutInflater().inflate(R.layout.dialog_progress,
				null);

		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setView(vg);
		alertDialog.setCancelable(true);
		alertDialog.show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (processGotoDashboard != null)
					processGotoDashboard.cancel(true);

				processGotoDashboard = new ProcessGotoDashboard();
				processGotoDashboard.execute();

				// alertDialog.cancel();
			}
		}, 900);

	}

	ProcessGetVendors processGetVendors;
	public class ProcessGetVendors extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			if (!alertDialog.isShowing())
				alertDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			SystemClock.sleep(1000 * 2);

			//let im: String? = member?.id_member
            //var url = "\(Constant.getAPIVendor())&action=get_vendor_bytype&im=\(im!)&tp=all"
            
			try {
				String url = Constant.URI_BASE_API
						+ "vendor_api.php?action=get_vendor_bytype";
				String data = "&im=" + member.getIdMember() + "&tp=all";
				
				Utils.log("uri: "+url);
				Utils.log("data: "+data);
				
				String response = PushServer.getResponsePush(url, data);
				JSONObject json = new JSONObject(response);
				if ("200".equals(json.getString("code"))) {
					JSONArray jArr = json.getJSONArray("result");
					SharedPref.saveResponseVendorPref(ChooseVendorPage.this, jArr.toString());
				}
				
			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (alertDialog.isShowing())
				alertDialog.cancel();

			reloadVendorBySharedPref();
		}

	}
	
	ProcessGotoDashboard processGotoDashboard;

	public class ProcessGotoDashboard extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			if (!alertDialog.isShowing())
				alertDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			SystemClock.sleep(1000 * 2);

			// var prefVendor = Constant.getVendorMember()
	        
	        //var idm = member?.id_member
	        //var url = Constant.getAPIMember()
	        //url = "\(url)&action=insert_pref_vendor&im=\(idm!)&pv=\(prefVendor)"
	       
			try {
				// prepare push API wishlist vendor click
				
				String prefVendor = "";
				ArrayList<VendorModel> models = adapter.getModels();
				for (int k=0; k<models.size(); k++) {
					VendorModel v = models.get(k);
					
					if (v.isChecked()) {
						prefVendor = prefVendor + ""+v.getKode()+",";
					}
				}
				
				if (prefVendor.length()>1) {
					prefVendor = prefVendor.substring(0,  prefVendor.length()-1);
				}

				Utils.log("prefVendor: "+prefVendor);
				String url = Constant.URI_BASE_API
						+ "member_api.php?action=insert_pref_vendor";
				String data = "&im=" + member.getIdMember() + "&pv=" + prefVendor 
						+ "&dt=" + Utils.getDateNow();
				
				Utils.log("uri: "+url);
				Utils.log("data: "+data);
				
				PushServer.getResponsePush(url, data);
				
			} catch (Exception e) {
			}
			
			try{
				// update member preferences
				String uri = Constant.URI_BASE_API+"member_api.php?action=get";
				String data = "&im="+member.getIdMember();
				
				String repsMember = PushServer.getResponsePush(uri, data);
				JSONObject json = new JSONObject(repsMember);
				if ("200".equals(json.getString("code"))) {
					JSONObject jData = json.getJSONArray("result").getJSONObject(0);
					Utils.log("Data response login; "+jData.toString());
					
					SharedPref.saveResponseUserLoginPref(ChooseVendorPage.this, jData.toString());
					SharedPref.saveStatusPref(ChooseVendorPage.this, "1");
				}
			}
			catch(Exception e) {}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (alertDialog.isShowing())
				alertDialog.cancel();

			Utils.toast(ChooseVendorPage.this, "Process signup done..");
			SharedPref.saveStatusPref(ChooseVendorPage.this, "1");
			Intent i = new Intent(ChooseVendorPage.this,
					MainActivity.class);
			startActivity(i);

		}

	}

	public void checkColorNextButton() {
		layoutBottom.setBackgroundResource(R.color.blue);
		btnNext.setBackgroundResource(R.color.blue);

		if (adapter.countCheckedItem() > 0) {
			btnNext.setBackgroundResource(R.color.orange);
			layoutBottom.setBackgroundResource(R.color.orange);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if (processGotoDashboard!=null)
			processGotoDashboard.cancel(true);
		
		if (processGetVendors!=null)
			processGetVendors.cancel(true);
	}

}
