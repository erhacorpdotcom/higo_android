package com.higofiturindonesia.higo.page;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.higofiturindonesia.higo.R;
import com.higofiturindonesia.higo.adapter.GridCategoryAdapter;
import com.higofiturindonesia.higo.model.UserModel;
import com.higofiturindonesia.higo.util.Constant;
import com.higofiturindonesia.higo.util.PushServer;
import com.higofiturindonesia.higo.util.SharedPref;
import com.higofiturindonesia.higo.util.Utils;

public class ChooseCategoryPage extends Activity {

	GridView gridView;
	LinearLayout layoutBottom;
	Button btnNext;

	GridCategoryAdapter adapter;
	boolean[] checked = new boolean[] { false, false, false, false, false,
			false, false, false };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_category);

		Utils.setPolicyThread();
		SharedPref.saveStatusPref(this, "2");
		
		layoutBottom = (LinearLayout) findViewById(R.id.layoutBottom);
		btnNext = (Button) findViewById(R.id.next);

		gridView = (GridView) findViewById(R.id.gridView);
		adapter = new GridCategoryAdapter(this, checked);
		gridView.setAdapter(adapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (position > -1)
					adapter.setChecked(position);

				checkColorNextButton();
			}
		});

		layoutBottom.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clickActionBottom();
				
			}
		});
		
		btnNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clickActionBottom();
				
			}
		});

	}
	
	public void clickActionBottom() {
		if (adapter.countCheckedItem() > 0) {
			preparePushWishlist();
		} else {
			Utils.toast(ChooseCategoryPage.this,
					"Category not selected yet...");
		}
	}

	UserModel member = new UserModel();

	void preparePushWishlist() {
		// var url = Constant.getAPIMember()
		// var em = member?.email as String?
		// var dt = NSDate().formattedWith("yyyy-MM-dd HH:mm:ss")
		// //Constant.currentTimeMillis()
		// dt = Constant.URLEncodedString(dt)

		// url =
		// "\(url)&action=insert_wishlist_category&em=\(em!)&c=\(c)&mc=\(mc)&dt=\(dt)"

		member = UserModel.UserModelFromJson(this);
		if (member.getIdMember() != null) {
			Utils.log("get member id: " + member.getIdMember());
			showDialogProcess();
		}

	}

	AlertDialog alertDialog;

	public void showDialogProcess() {

		// check tanggal berangkat dan tanggal pulang

		View vg = this.getLayoutInflater().inflate(R.layout.dialog_progress,
				null);

		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setView(vg);
		alertDialog.setCancelable(true);
		alertDialog.show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (processWishlist != null)
					processWishlist.cancel(true);

				processWishlist = new ProcessWishlist();
				processWishlist.execute();

				// alertDialog.cancel();
			}
		}, 900);

	}

	ProcessWishlist processWishlist;

	public class ProcessWishlist extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			if (!alertDialog.isShowing())
				alertDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			SystemClock.sleep(1000 * 2);

			// http://higoapps.com/api/member_api.php?&action=insert_wishlist_category
			// &action=insert_wishlist_category&em=\(em!)&c=\(c)&mc=\(mc)&dt=\(dt)"
			try {
				String categ = "";
				String multiCateg = "";

				boolean[] getChecked = adapter.getChecked();
				int j = 0;
				for (int k=0; k<getChecked.length; k++) {
					j = k + 1;
					
					if (getChecked[k]) {
						if (adapter.countCheckedItem()==1) {
							categ = ""+j;
						}
						multiCateg = multiCateg + ""+j+",";
					}
				}
				
				if (multiCateg.length()>1) {
					multiCateg = multiCateg.substring(0,  multiCateg.length()-1);
				}
				
				Utils.log("categ: "+categ);
				Utils.log("multi categ: "+multiCateg);
				
				member.setTempCateg(categ);
				member.setTempMultiCateg(multiCateg);
				
				String url = Constant.URI_BASE_API
						+ "member_api.php?action=insert_wishlist_category";
				String data = "&em=" + member.getIdMember() + "&c=" + categ
						+ "&mc=" + multiCateg + "&dt=" + Utils.getDateNow();
				
				Utils.log("uri: "+url);
				Utils.log("data: "+data);
				
				PushServer.getResponsePush(url, data);
				
			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (alertDialog.isShowing())
				alertDialog.cancel();

			Utils.toast(ChooseCategoryPage.this, "Process wishlist success..");
			Intent i = new Intent(ChooseCategoryPage.this,
					ChooseVendorPage.class);
			startActivity(i);

		}

	}

	public void checkColorNextButton() {
		layoutBottom.setBackgroundResource(R.color.blue);
		btnNext.setBackgroundResource(R.color.blue);

		if (adapter.countCheckedItem() > 0) {
			btnNext.setBackgroundResource(R.color.orange);
			layoutBottom.setBackgroundResource(R.color.orange);
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Utils.log("onBackPressed()onBackPressed(): ");

		return;
		// super.onBackPressed();
		// finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if (processWishlist!=null)
			processWishlist.cancel(true);
	}

}
