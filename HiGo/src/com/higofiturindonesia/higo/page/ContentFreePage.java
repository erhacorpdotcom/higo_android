package com.higofiturindonesia.higo.page;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.higofiturindonesia.higo.R;
import com.higofiturindonesia.higo.adapter.ContentFreeAdapter;
import com.higofiturindonesia.higo.model.ContentModel;
import com.higofiturindonesia.higo.model.UserModel;
import com.higofiturindonesia.higo.util.Constant;
import com.higofiturindonesia.higo.util.PushServer;
import com.higofiturindonesia.higo.util.SharedPref;
import com.higofiturindonesia.higo.util.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;

public class ContentFreePage extends Activity {

	ContentFreeAdapter adapter;
	ListView listView;
	ArrayList<ContentModel> contents = new ArrayList<ContentModel>();
	ImageView btnBack;
	
	UserModel member = new UserModel();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.content_free);

		Utils.setPolicyThread();
		member = UserModel.UserModelFromJson(this);
		if (member.getIdMember()==null)
			member.setIdMember("8"); // test dummy member
		
		listView = (ListView) findViewById(R.id.listView);
		adapter = new ContentFreeAdapter(this, contents);
		listView.setAdapter(adapter);
		
		btnBack = (ImageView) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		
		// check if exist cache data content free
		String resp = SharedPref.getResponseContentFreePref(this);
		try {
			JSONArray jArr = new JSONArray(resp);
			contents = new ArrayList<ContentModel>();
			
			for (int j=0; j<jArr.length(); j++) {
				JSONObject jData = jArr.getJSONObject(j);
				ContentModel c = ContentModel.setContentFromModelJSON(jData.toString());
				
				contents.add(c);
			}
			
			adapter.setItemData(contents);
		}
		catch(Exception e) {}
		
		// loading content
		//showDialogProcess();
		
		if (processContentFree != null)
			processContentFree.cancel(true);

		processContentFree = new ProcessContentFree();
		processContentFree.execute();
	}
	
	AlertDialog alertDialog;

	public void showDialogProcess() {

		// check tanggal berangkat dan tanggal pulang

		View vg = this.getLayoutInflater().inflate(R.layout.dialog_progress,
				null);

		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setView(vg);
		alertDialog.setCancelable(true);
		alertDialog.show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (processContentFree != null)
					processContentFree.cancel(true);

				processContentFree = new ProcessContentFree();
				processContentFree.execute();

				// alertDialog.cancel();
			}
		}, 900);

	}
	
	
	ProcessContentFree processContentFree;

	public class ProcessContentFree extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			//if (!alertDialog.isShowing())
			//	alertDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			SystemClock.sleep(800 * 2);
			
			try {
				// http://higoapps.com/api/content_api.php?action=get_content_free&im=8
				String url = Constant.URI_BASE_API
						+ "content_api.php?action=get_content_free";
				String data = "&im=" + member.getIdMember();
				
				Utils.log("uri: "+url);
				Utils.log("data: "+data);
				
				String response = PushServer.getResponsePush(url, data);
				JSONObject json = new JSONObject(response);
				if ("200".equals(json.getString("code"))) {
					JSONArray jArr = json.getJSONArray("result");
					contents = new ArrayList<ContentModel>();
					
					SharedPref.saveResponseContentFreePref(ContentFreePage.this, jArr.toString());
					
					for (int j=0; j<jArr.length(); j++) {
						JSONObject jData = jArr.getJSONObject(j);
						ContentModel c = ContentModel.setContentFromModelJSON(jData.toString());
						
						contents.add(c);
					}
				}
				
			} catch (Exception e) {
			}
			

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//if (alertDialog.isShowing())
			//	alertDialog.cancel();

			adapter.setItemData(contents);
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if (processContentFree != null)
			processContentFree.cancel(true);

	}

}
