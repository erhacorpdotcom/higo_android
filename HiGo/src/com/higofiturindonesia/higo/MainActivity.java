package com.higofiturindonesia.higo;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.higofiturindonesia.higo.adapter.GridDashboardAdapter;
import com.higofiturindonesia.higo.extend.CircleImageView;
import com.higofiturindonesia.higo.extend.HorzScrollWithListMenu;
import com.higofiturindonesia.higo.extend.HorzScrollWithListMenu.ClickListenerForScrolling;
import com.higofiturindonesia.higo.extend.MyHorizontalScrollView;
import com.higofiturindonesia.higo.model.UserModel;
import com.higofiturindonesia.higo.model.VendorModel;
import com.higofiturindonesia.higo.page.ContentFreePage;
import com.higofiturindonesia.higo.page.ProfilePage;
import com.higofiturindonesia.higo.util.Constant;
import com.higofiturindonesia.higo.util.ImageLoader;
import com.higofiturindonesia.higo.util.PushServer;
import com.higofiturindonesia.higo.util.SharedPref;
import com.higofiturindonesia.higo.util.Utils;

public class MainActivity extends Activity {

	MyHorizontalScrollView scrollView;
	View menu;
	View app;

	ImageView btnSlide, btnSearch;
	boolean menuOut = false;
	Handler handler = new Handler();
	int btnWidth;

	GridView gridView;
	ArrayList<VendorModel> vendors = new ArrayList<VendorModel>();
	GridDashboardAdapter gridAdapter;

	ImageButton btnSubscription, btnSingleIssue, btnWishlis;
	ClickListenerForScrolling leftMenuListener;
	UserModel member = new UserModel();

	Button home, free, setting, category;
	RelativeLayout layoutProfile;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		LayoutInflater inflater = LayoutInflater.from(this);
		setContentView(inflater.inflate(R.layout.activity_main, null));

		scrollView = (MyHorizontalScrollView) findViewById(R.id.myScrollView);
		menu = findViewById(R.id.layoutLeft);

		home = (Button) findViewById(R.id.home);
		free = (Button) findViewById(R.id.free);
		setting = (Button) findViewById(R.id.setting);
		category = (Button) findViewById(R.id.categories);

		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (leftMenuListener.isMenuOut())
					leftMenuListener.toggleMenuSlide();
				
			}
		});

		free.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (leftMenuListener.isMenuOut())
					leftMenuListener.toggleMenuSlide();

				Intent i = new Intent(MainActivity.this, ContentFreePage.class);
				startActivity(i);
			}
		});

		setting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (leftMenuListener.isMenuOut())
					leftMenuListener.toggleMenuSlide();
				
			}
		});

		category.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (leftMenuListener.isMenuOut())
					leftMenuListener.toggleMenuSlide();
				

			}
		});

		// "foto_member":"upload\/1436431303128.jpg"
		CircleImageView circularImage = (CircleImageView) findViewById(R.id.profile_image);
		String uriProfileImage = "";
		try {
			member = UserModel.UserModelFromJson(this);
			uriProfileImage = member.getFotoMember();
			if (uriProfileImage.length() > 0) {
				ImageLoader img = new ImageLoader(this);
				img.DisplayImage(uriProfileImage, R.drawable.profile,
						circularImage);
			}
		} catch (Exception e) {
		}

		TextView profileName = (TextView) findViewById(R.id.profile_name);
		profileName.setText("");
		if (member != null && member.getFirstName().trim().length() > 0) {
			String fullName = member.getFirstName().trim();
			if (member.getMiddleName() != null
					&& member.getMiddleName().trim().length() > 0)
				fullName += " " + member.getMiddleName().trim();

			if (member.getLastName() != null
					&& member.getLastName().trim().length() > 0)
				fullName += " " + member.getLastName().trim();

			profileName.setText("" + fullName);
		}

		layoutProfile = (RelativeLayout) findViewById(R.id.layoutProfile);
		layoutProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (leftMenuListener.isMenuOut())
					leftMenuListener.toggleMenuSlide();
				
				startActivityForResult(new Intent(MainActivity.this, ProfilePage.class), 911);
			}
		});
		
		app = inflater.inflate(R.layout.horz_scroll_app, null);
		ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.tabBar);

		// ListView listView = (ListView) app.findViewById(R.id.list);
		// ViewUtils.initListView(this, listView, "Item ", 10,
		// android.R.layout.simple_list_item_1);

		btnSubscription = (ImageButton) app.findViewById(R.id.btnSubscription);
		btnSingleIssue = (ImageButton) app.findViewById(R.id.btnSingleIssue);
		btnWishlis = (ImageButton) app.findViewById(R.id.btnWishlist);

		btnSubscription.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btnSubscription.setBackgroundResource(R.drawable.subshover);
				btnSingleIssue.setBackgroundResource(R.drawable.singleissue);
				btnWishlis.setBackgroundResource(R.drawable.wishlist);

				clickMySubscription();
			}
		});

		btnSingleIssue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btnSubscription.setBackgroundResource(R.drawable.subs);
				btnSingleIssue
						.setBackgroundResource(R.drawable.singleissuehover);
				btnWishlis.setBackgroundResource(R.drawable.wishlist);

				clickMySingleIssue();
			}
		});

		btnWishlis.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btnSubscription.setBackgroundResource(R.drawable.subs);
				btnSingleIssue.setBackgroundResource(R.drawable.singleissue);
				btnWishlis.setBackgroundResource(R.drawable.wishlisthover);

				clickMyWishList();
			}
		});

		btnWishlis.setBackgroundResource(R.drawable.wishlisthover);

		gridView = (GridView) app.findViewById(R.id.gridView);
		gridView.setNumColumns(4);
		vendors = new ArrayList<VendorModel>();
		gridAdapter = new GridDashboardAdapter(this, vendors, true);
		gridView.setAdapter(gridAdapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				clickItemGrid(position);
			}
		});

		// setDummyData();
		clickMyWishList();

		gridAdapter.setItemData(vendors);

		btnSearch = (ImageView) tabBar.findViewById(R.id.btnSearch);
		btnSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		btnSlide = (ImageView) tabBar.findViewById(R.id.btnSlide);
		leftMenuListener = new HorzScrollWithListMenu.ClickListenerForScrolling(
				scrollView, menu);
		btnSlide.setOnClickListener(leftMenuListener);

		// Create a transparent view that pushes the other views in the HSV to
		// the right.
		// This transparent view allows the menu to be shown when the HSV is
		// scrolled.
		View transparent = new TextView(this);
		transparent.setBackgroundColor(android.R.color.transparent);

		final View[] children = new View[] { transparent, app };

		// Scroll to app (view[1]) when layout finished.
		int scrollToViewIdx = 1;
		scrollView.initViews(children, scrollToViewIdx,
				new HorzScrollWithListMenu.SizeCallbackForMenu(btnSlide));
	}

	void clickItemGrid(int position) {
		if (leftMenuListener != null) {
			Log.e("TAG MainActivity",
					"showAlertOnBack(): leftMenuListener.isMenuOut() "
							+ leftMenuListener.isMenuOut());

			if (leftMenuListener.isMenuOut()) {
				leftMenuListener.toggleMenuSlide();
				return;
			}
		}

		// gridAdapter.toggleChecked(position);

	}

	void clickMySubscription() {

		if (processGetSingleIssue != null)
			processGetSingleIssue.cancel(true);
		
		if (processGetWishlist != null)
			processGetWishlist.cancel(true);
		
		vendors = new ArrayList<VendorModel>();
		gridAdapter.setItemData(vendors);

		String getWishlistPref = SharedPref.getResponseSubscriptionPref(this);
		try {
			if (getWishlistPref.length() > 0) {
				JSONArray jArr = new JSONArray(getWishlistPref);

				for (int k = 0; k < jArr.length(); k++) {
					VendorModel v = VendorModel.setVendorFromModelJSON(jArr
							.get(k).toString());
					vendors.add(v);
				}
			}
		} catch (Exception e) {
		}

		gridAdapter.setItemData(vendors);

		if (processGetSubscription != null)
			processGetSubscription.cancel(true);

		processGetSubscription = new ProcessGetSubscription();
		processGetSubscription.execute();

	}

	void clickMyWishList() {
		
		if (processGetSingleIssue != null)
			processGetSingleIssue.cancel(true);
		
		if (processGetSubscription != null)
			processGetSubscription.cancel(true);
		
		vendors = new ArrayList<VendorModel>();
		gridAdapter.setItemData(vendors);

		String getWishlistPref = SharedPref.getResponseWishlistPref(this);
		try {
			if (getWishlistPref.length() > 0) {
				JSONArray jArr = new JSONArray(getWishlistPref);

				for (int k = 0; k < jArr.length(); k++) {
					VendorModel v = VendorModel.setVendorFromModelJSON(jArr
							.get(k).toString());
					vendors.add(v);
				}
			}
		} catch (Exception e) {
		}

		gridAdapter.setItemData(vendors);

		if (processGetWishlist != null)
			processGetWishlist.cancel(true);

		processGetWishlist = new ProcessGetWishlist();
		processGetWishlist.execute();

	}

	void clickMySingleIssue() {
		
		if (processGetSubscription != null)
			processGetSubscription.cancel(true);
		
		if (processGetWishlist != null)
			processGetWishlist.cancel(true);
		
		vendors = new ArrayList<VendorModel>();
		gridAdapter.setItemData(vendors);

		String getWishlistPref = SharedPref.getResponseSingleIssuePref(this);
		try {
			if (getWishlistPref.length() > 0) {
				JSONArray jArr = new JSONArray(getWishlistPref);

				for (int k = 0; k < jArr.length(); k++) {
					VendorModel v = VendorModel.setVendorFromModelJSON(jArr
							.get(k).toString());
					vendors.add(v);
				}
			}
		} catch (Exception e) {
		}

		gridAdapter.setItemData(vendors);

		if (processGetSingleIssue != null)
			processGetSingleIssue.cancel(true);

		processGetSingleIssue = new ProcessGetSingleIssue();
		processGetSingleIssue.execute();
	}

	void setDummyData() {

		vendors = new ArrayList<VendorModel>();
		gridAdapter.setItemData(vendors);

		for (int i = 0; i < 30; i++) {
			VendorModel m = new VendorModel();
			m.setId("" + i);
			m.setIcon(R.drawable.logo_05);
			m.setChecked(false);
			vendors.add(m);
		}
	}

	void reloadDataVendors() {
		gridAdapter.setItemData(vendors);
	}

	ProcessGetSubscription processGetSubscription;

	public class ProcessGetSubscription extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Utils.toast(MainActivity.this, "please wait...");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			SystemClock.sleep(800);

			try {
				String url = Constant.URI_BASE_API
						+ "trans_api.php?action=get_vendor_subscriber_colec";
				String data = "&im=" + member.getIdMember();

				Utils.log("uri: " + url);
				Utils.log("data: " + data);

				String response = PushServer.getResponsePush(url, data);
				JSONObject json = new JSONObject(response);
				vendors = new ArrayList<VendorModel>();

				if ("200".equals(json.getString("code"))) {
					JSONArray jArr = json.getJSONArray("result");
					SharedPref.saveResponseSubscriptionPref(MainActivity.this,
							jArr.toString());

					for (int k = 0; k < jArr.length(); k++) {
						VendorModel v = VendorModel.setVendorFromModelJSON(jArr
								.get(k).toString());
						vendors.add(v);
					}
				}

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			reloadDataVendors();
		}

	}

	ProcessGetSingleIssue processGetSingleIssue;

	public class ProcessGetSingleIssue extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Utils.toast(MainActivity.this, "please wait...");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			SystemClock.sleep(800);

			try {
				String url = Constant.URI_BASE_API
						+ "trans_api.php?action=get_vendor_single_colec";
				String data = "&im=" + member.getIdMember();

				Utils.log("uri: " + url);
				Utils.log("data: " + data);

				String response = PushServer.getResponsePush(url, data);
				JSONObject json = new JSONObject(response);
				vendors = new ArrayList<VendorModel>();

				if ("200".equals(json.getString("code"))) {
					JSONArray jArr = json.getJSONArray("result");
					SharedPref.saveResponseSingleIssuePref(MainActivity.this,
							jArr.toString());

					for (int k = 0; k < jArr.length(); k++) {
						VendorModel v = VendorModel.setVendorFromModelJSON(jArr
								.get(k).toString());
						vendors.add(v);
					}
				}

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			reloadDataVendors();
		}

	}

	ProcessGetWishlist processGetWishlist;

	public class ProcessGetWishlist extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Utils.toast(MainActivity.this, "please wait...");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			SystemClock.sleep(800);

			try {
				String url = Constant.URI_BASE_API
						+ "trans_api.php?action=get_vendor_wishlist";
				String data = "&im=" + member.getIdMember();

				Utils.log("uri: " + url);
				Utils.log("data: " + data);

				String response = PushServer.getResponsePush(url, data);
				JSONObject json = new JSONObject(response);
				vendors = new ArrayList<VendorModel>();

				if ("200".equals(json.getString("code"))) {
					JSONArray jArr = json.getJSONArray("result");
					SharedPref.saveResponseWishListPref(MainActivity.this,
							jArr.toString());

					for (int k = 0; k < jArr.length(); k++) {
						VendorModel v = VendorModel.setVendorFromModelJSON(jArr
								.get(k).toString());
						vendors.add(v);
					}
				}

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			reloadDataVendors();
		}

	}

	@Override
	public void onBackPressed() {
		showAlertOnBack();
	}

	public void showAlertOnBack() {

		if (leftMenuListener != null) {
			Log.e("TAG MainActivity",
					"showAlertOnBack(): leftMenuListener.isMenuOut() "
							+ leftMenuListener.isMenuOut());

			if (leftMenuListener.isMenuOut()) {
				leftMenuListener.toggleMenuSlide();
				return;
			}
		}

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setMessage("Are you sure to exit?");
		builder1.setCancelable(true);
		builder1.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						try {

						} catch (Exception e) {
						}

						finish();
					}
				});

		builder1.setNegativeButton("No", null);
		AlertDialog alert11 = builder1.create();
		alert11.show();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if (processGetSingleIssue != null)
			processGetSingleIssue.cancel(true);
		
		if (processGetSubscription != null)
			processGetSubscription.cancel(true);
		
		if (processGetWishlist != null)
			processGetWishlist.cancel(true);

	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		
		Utils.log("MainAcitvity: requestCode: "+requestCode);
		
		try {
			// logout
			if (requestCode==911) {
				if (resultCode==Activity.RESULT_OK) {
					SharedPref.clearAllPreferenceRelated(MainActivity.this);
					
					Intent i = new Intent(MainActivity.this, Splashscreen.class);
					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(i);
					finish();
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}

}
