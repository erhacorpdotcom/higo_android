package com.higofiturindonesia.higo;

import org.json.JSONObject;

import com.higofiturindonesia.higo.util.Constant;
import com.higofiturindonesia.higo.util.PushServer;
import com.higofiturindonesia.higo.util.SharedPref;
import com.higofiturindonesia.higo.util.Utils;
import com.higofiturindonesia.higo.util.Widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class LoginPage extends Activity {

	Button login, forgot;
	ImageView back;
	EditText inputEmail, inputPassword;
	ProgressBar progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_login);

		Utils.setPolicyThread();

		back = (ImageView) findViewById(R.id.back);
		login = (Button) findViewById(R.id.login);
		forgot = (Button) findViewById(R.id.forgot);

		inputEmail = (EditText) findViewById(R.id.email);
		inputPassword = (EditText) findViewById(R.id.password);

		progress = (ProgressBar) findViewById(R.id.progress);
		progress.setVisibility(View.GONE);

		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// startActivity(new Intent(LoginPage.this,
				// MainActivity.class));
				if (checkProses())
					showDialogProcess();
			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		forgot.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialogForgot();
			}
		});

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				Widget.hideKeyboard(LoginPage.this);
			}
		}, 800);
	}

	public void showDialogForgot() {

		// check tanggal berangkat dan tanggal pulang

		View vg = this.getLayoutInflater().inflate(R.layout.dialog_forgot_password,
				null);
		
		final TextView status = (TextView) vg.findViewById(R.id.status);
		final TextView senddone = (TextView) vg.findViewById(R.id.senddone);
		senddone.setVisibility(View.GONE);
		
		final TextView send = (TextView) vg.findViewById(R.id.send);
		final EditText email = (EditText) vg.findViewById(R.id.email);
		
		send.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				status.setVisibility(View.GONE);
				email.setVisibility(View.GONE);
				send.setVisibility(View.GONE);
				
				senddone.setVisibility(View.VISIBLE);
				
			}
		});
		
		ImageView backButton = (ImageView) vg.findViewById(R.id.backButton);
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.cancel();
			}
		});

		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setView(vg);
		alertDialog.setCancelable(true);
		alertDialog.show();

	}
	
	boolean checkProses() {
		String user = inputEmail.getText().toString().trim();
		String pwd = inputPassword.getText().toString().trim();

		if (user.length() < 2) {
			Utils.toast(this, "Username invalid...");
			return false;
		}

		if (pwd.length() < 2) {
			Utils.toast(this, "Password invalid...");
			return false;
		}

		return true;
	}

	AlertDialog alertDialog;

	public void showDialogProcess() {

		// check tanggal berangkat dan tanggal pulang

		View vg = this.getLayoutInflater().inflate(R.layout.dialog_progress,
				null);

		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setView(vg);
		alertDialog.setCancelable(true);
		alertDialog.show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (prosesLogin != null)
					prosesLogin.cancel(true);

				prosesLogin = new ProsesLogin();
				prosesLogin.execute();

				// alertDialog.cancel();
			}
		}, 900);

	}

	ProsesLogin prosesLogin;

	public class ProsesLogin extends AsyncTask<Void, Void, Void> {

		boolean check = false;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			if (!alertDialog.isShowing())
				alertDialog.show();

			check = false;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			SystemClock.sleep(1000 * 2);

			// http://higoapps.com/api/member_api.php?action=log_in&em=rully.hasibuan@gmail.com&pass=123456

			String user = inputEmail.getText().toString().trim();
			String pwd = inputPassword.getText().toString().trim();

			String uri = Constant.URI_BASE_API
					+ "member_api.php?&action=log_in";
			String data = "&em=" + user + "&pass=" + pwd;

			Utils.log("uri: " + uri);
			Utils.log("data: " + data);

			try {
				String resp = PushServer.getResponsePush(uri, data);
				JSONObject json = new JSONObject(resp);
				if ("200".equals(json.getString("code"))) {
					check = true;
					
					JSONObject jData = json.getJSONArray("result").getJSONObject(0);
					Utils.log("Data response login; "+jData.toString());
					
					SharedPref.saveResponseUserLoginPref(LoginPage.this, jData.toString());
					SharedPref.saveStatusPref(LoginPage.this, "1");
				}
				else {
					SharedPref.saveResponseUserLoginPref(LoginPage.this, "");
					SharedPref.saveStatusPref(LoginPage.this, "0");
				}
				
			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (alertDialog.isShowing())
				alertDialog.cancel();
			
			if (check) {
				Utils.toast(LoginPage.this, "Login success");
				Intent i = new Intent(LoginPage.this, MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
				
				setResult(Activity.RESULT_OK);
				finish();
			} else {
				Utils.toast(LoginPage.this, "Login failed...");
			}

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if (prosesLogin!=null) {
			prosesLogin.cancel(true);
		}
	}
}
