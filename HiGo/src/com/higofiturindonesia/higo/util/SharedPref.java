package com.higofiturindonesia.higo.util;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {

	public static final String NAME_PREF = "higoapps_pref";

	public static final String KEY_EMAIL_PREF = "email_pref";
	public static final String KEY_NAME_PREF = "name_pref";
	public static final String KEY_LATITUDE_PREF = "latitude_pref";
	public static final String KEY_ADDRESS_PREF = "address_pref";
	public static final String KEY_STATUS_PREF = "status_pref";
	public static final String KEY_LOCK_PREF = "lock_pref";
	public static final String KEY_PIN_PREF = "pin_pref";
	public static final String KEY_MSISDN_PREF = "msisdn_pref";
	public static final String KEY_PASSWORD_PREF = "password_pref";
	public static final String KEY_TIPEADMIN_PREF = "tipeadmin_pref";

	public static final String KEY_RESPONSE_API_PREF = "response_api_pref";
	public static final String KEY_RESPONSE_USERLOGIN_PREF = "response_userlogin_pref";
	public static final String KEY_RESPONSE_VENDOR_PREF = "response_vendor_pref";
	
	public static final String KEY_RESPONSE_WISHLIST_PREF = "response_wishlist_pref";
	public static final String KEY_RESPONSE_SINGLEISSUE_PREF = "response_singleissue_pref";
	public static final String KEY_RESPONSE_SUBSCRIBE_PREF = "response_subscribe_pref";
	public static final String KEY_RESPONSE_CONTENTFREE_PREF = "response_contentfree_pref";

	public static void clearAllPreferenceRelated(Context ctx) {
		SharedPref.saveResponseUserLoginPref(ctx, "");
		SharedPref.saveStatusPref(ctx, "0");
		SharedPref.saveResponseSingleIssuePref(ctx, "");
		SharedPref.saveResponseSubscriptionPref(ctx, "");
		SharedPref.saveResponseWishListPref(ctx, "");
	}
	
	public static void saveAllPref(Context ctx, ArrayList<String> data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_EMAIL_PREF, data.get(0).trim());
		edit.putString(KEY_NAME_PREF, data.get(1).trim());
		edit.putString(KEY_LATITUDE_PREF, data.get(2).trim());
		edit.putString(KEY_ADDRESS_PREF, data.get(3).trim());
		edit.putString(KEY_STATUS_PREF, data.get(4).trim());
		edit.putString(KEY_RESPONSE_API_PREF, data.get(5).trim());

		edit.putString(KEY_LOCK_PREF, data.get(6).trim());
		edit.putString(KEY_PIN_PREF, data.get(7).trim());
		edit.putString(KEY_PASSWORD_PREF, data.get(8).trim());

		edit.commit();
	}

	public static ArrayList<String> getAllPref(Context ctx) {
		ArrayList<String> data = new ArrayList<String>();
		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		data.add(sharedPref.getString(KEY_EMAIL_PREF, ""));
		data.add(sharedPref.getString(KEY_NAME_PREF, ""));
		data.add(sharedPref.getString(KEY_LATITUDE_PREF, ""));
		data.add(sharedPref.getString(KEY_ADDRESS_PREF, ""));
		data.add(sharedPref.getString(KEY_STATUS_PREF, ""));
		data.add(sharedPref.getString(KEY_RESPONSE_API_PREF, ""));

		data.add(sharedPref.getString(KEY_LOCK_PREF, ""));
		data.add(sharedPref.getString(KEY_PIN_PREF, ""));
		data.add(sharedPref.getString(KEY_PASSWORD_PREF, ""));

		return data;
	}

	public static void saveNoHPPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_MSISDN_PREF, data);

		edit.commit();
	}

	public static String getNoHPPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_MSISDN_PREF, "");
	}

	public static void saveEmailPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_EMAIL_PREF, data);

		edit.commit();
	}

	public static String getEmailPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_EMAIL_PREF, "");
	}

	public static void saveNamePref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_NAME_PREF, data);

		edit.commit();
	}

	public static String getNamePref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_NAME_PREF, "");
	}

	public static void saveLatitudePref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_LATITUDE_PREF, data);

		edit.commit();
	}

	public static String getLatitudePref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_LATITUDE_PREF, "");
	}

	public static void saveAddressPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_ADDRESS_PREF, data);

		edit.commit();
	}

	public static String getAddressPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_ADDRESS_PREF, "");
	}

	public static void saveResponseAPIPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_RESPONSE_API_PREF, data);

		edit.commit();
	}

	public static String getResponseAPIPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_RESPONSE_API_PREF, "");
	}

	public static void saveResponseUserLoginPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_RESPONSE_USERLOGIN_PREF, data);

		edit.commit();
	}

	public static String getResponseUserLoginPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_RESPONSE_USERLOGIN_PREF, "");
	}
	
	public static void saveResponseVendorPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_RESPONSE_VENDOR_PREF, data);

		edit.commit();
	}

	public static String getResponseVendorPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_RESPONSE_VENDOR_PREF, "");
	}

	public static void saveResponseWishListPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_RESPONSE_WISHLIST_PREF, data);

		edit.commit();
	}

	public static String getResponseWishlistPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_RESPONSE_WISHLIST_PREF, "");
	}
	
	public static void saveResponseSubscriptionPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_RESPONSE_SUBSCRIBE_PREF, data);

		edit.commit();
	}

	public static String getResponseSubscriptionPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_RESPONSE_SUBSCRIBE_PREF, "");
	}
	
	public static void saveResponseSingleIssuePref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_RESPONSE_SINGLEISSUE_PREF, data);

		edit.commit();
	}

	public static String getResponseSingleIssuePref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_RESPONSE_SINGLEISSUE_PREF, "");
	}
	
	public static void saveResponseContentFreePref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_RESPONSE_CONTENTFREE_PREF, data);

		edit.commit();
	}

	public static String getResponseContentFreePref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_RESPONSE_CONTENTFREE_PREF, "");
	}
	
	public static void saveLockPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_LOCK_PREF, data);

		edit.commit();
	}

	public static String getLockPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_LOCK_PREF, "");
	}

	public static void savePinPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_PIN_PREF, data);

		edit.commit();
	}

	public static String getPinPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_PIN_PREF, "");
	}

	public static void savePasswordPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_PASSWORD_PREF, data);

		edit.commit();
	}

	public static String getPasswordPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_PASSWORD_PREF, "");
	}

	public static void saveStatusPref(Context ctx, String data) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();

		edit.putString(KEY_STATUS_PREF, data);

		edit.commit();
	}

	public static String getStatusPref(Context ctx) {

		SharedPreferences sharedPref = ctx.getSharedPreferences(NAME_PREF,
				Context.MODE_PRIVATE);

		return sharedPref.getString(KEY_STATUS_PREF, "");
	}
}
