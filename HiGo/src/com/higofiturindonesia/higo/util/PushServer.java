package com.higofiturindonesia.higo.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class PushServer {

	public static String getResponsePush(String url, String postData) {

		StringBuilder str = new StringBuilder();

		try {
			URLConnection connection = new URL(
					url.trim())
					.openConnection();
			
			// Http Method becomes POST
			connection.setDoOutput(true);

			String content = postData;
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");

			connection.addRequestProperty("Content-Length",
						""+content.getBytes().length);
					
			// Write body
			OutputStream output = connection.getOutputStream();
			output.write(content.getBytes());
			output.close();
			
			// get response
			BufferedReader rd = new BufferedReader(new InputStreamReader(connection
                    .getInputStream()));

            String line;
            //int linecount = 0;
            while ((line = rd.readLine()) != null) {
                str.append(line);
            }
            rd.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return str.toString();
	}
}
