package com.higofiturindonesia.higo.util;

public class Constant {

	public static final String URI_BASE = "http://higoapps.com/";
	public static final String URI_BASE_API = URI_BASE + "api/";
	
	public static final String[] CATEG_NAME = new String[]{"Travel", "News", "Women's Lifestyle",
		"Men's Lifestyle", "Sport", "Business & Finance", "Entertainment",
		"Science & Technology"};
	
}
