package com.higofiturindonesia.higo.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Widget {

	public static void tintWidget(Context ctx, View view, int color) {
        //Drawable wrappedDrawable = DrawableCompat.wrap(view.getBackground());
		Drawable wrappedDrawable = view.getBackground();
        DrawableCompat.setTint(wrappedDrawable, ctx.getResources().getColor(color));
        view.setBackgroundDrawable(wrappedDrawable);
    }
	
	public static void hideKeyboard(Activity ctx) {   
	    // Check if no view has focus:
	    View view = ctx.getCurrentFocus();
	    if (view != null) {
	        InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
	        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
}
