package com.higofiturindonesia.higo.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

public class Utils {

	public static String getDateNow() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateNow = formatter.format(currentDate.getTime());

		return dateNow;
	}
	
	public static String getFormatDate() {
		Random random = new Random(); // or new Random(someSeed);
		int value = 1500 + random.nextInt(500);
		
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String dateNow = formatter.format(currentDate.getTime());

		return dateNow + "" +value;
	}

	public static String getMonthYearOnly(String dateString) {
		String myFormat = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

		String res = "";

		try {
			Date dateObj = sdf.parse(dateString);
			SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy");
			res = formatter.format(dateObj.getTime());
		} catch (Exception e) {

		}
		
		return res;
	}
	
	public static String getMonthNameOnly() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MM");
		String month = formatter.format(currentDate.getTime());
		return month;
	}
	
	public static String getMonthNameFullOnly() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MMMM");
		String month = formatter.format(currentDate.getTime());
		return month;
	}

	public static String getYearOnly() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		String dateNow = formatter.format(currentDate.getTime());

		return dateNow;
	}

	public static String getDateOnly() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String dateNow = formatter.format(currentDate.getTime());

		return dateNow;
	}
	
	public static String getDayOnly() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("EEEE");
		String dateNow = formatter.format(currentDate.getTime());

		return dateNow;
	}
	
	public static int[] getScreenSize(Activity activity) {
		
		int[] screens = new int[]{0,0};
		
		WindowManager w = activity.getWindowManager();
		Display d = w.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		d.getMetrics(metrics);
		// since SDK_INT = 1;
		int widthPixels = metrics.widthPixels;
		int heightPixels = metrics.heightPixels;
		try {
		    // used when 17 > SDK_INT >= 14; includes window decorations (statusbar bar/menu bar)
		    widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
		    heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
		} catch (Exception ignored) {
		}
		try {
		    // used when SDK_INT >= 17; includes window decorations (statusbar bar/menu bar)
		    Point realSize = new Point();
		    Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
		    widthPixels = realSize.x;
		    heightPixels = realSize.y;
		} catch (Exception ignored) {
		}
		
		screens = new int[]{widthPixels, heightPixels};
		
		return screens;
	}
	
	public static boolean isValidEmail(CharSequence target) {
	    if (target == null) {
	        return false;
	    } else {
	        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	    }
	}
	
	public static String getIMEITelephone(Context ctx) {
		TelephonyManager tm = (TelephonyManager) ctx
				.getSystemService(Context.TELEPHONY_SERVICE);

		String res = "0000";

		try {
			// get IMEI
			res = tm.getDeviceId();
			// String phone = tm.getLine1Number();
		} catch (Exception e) {
		}

		return res;
	}
	
	public static String getUniqueImageFilenameDownloaded() {
		String res = "";

		Date currentDate = new Date();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd-HHmmss");
		res = formatter.format(currentDate.getTime()) + ".jpeg";

		return res;
	}
	
	public static Bitmap downloadBitmap(String url) {
		final AndroidHttpClient client = AndroidHttpClient
				.newInstance("Android");
		final HttpGet getRequest = new HttpGet(url);
		try {
			HttpResponse response = client.execute(getRequest);
			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				Log.w("ImageDownloader", "Error " + statusCode
						+ " while retrieving bitmap from " + url);
				return null;
			}

			final HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream inputStream = null;
				try {
					inputStream = entity.getContent();
					final Bitmap bitmap = BitmapFactory
							.decodeStream(inputStream);
					return bitmap;
				} finally {
					if (inputStream != null) {
						inputStream.close();
					}
					entity.consumeContent();
				}
			}
		} catch (Exception e) {
			// Could provide a more explicit error message for IOException or
			// IllegalStateException
			getRequest.abort();
			Log.w("ImageDownloader", "Error while retrieving bitmap from "
					+ url);
		} finally {
			if (client != null) {
				client.close();
			}
		}
		return null;
	}
	
	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static void call(Activity act, String phone) {
		Intent callIntent = new Intent(Intent.ACTION_DIAL);
		callIntent.setData(Uri.parse("tel:" + phone));
		act.startActivity(callIntent);
	}

	public static void showDialogEmail(final Activity act,
			ArrayList<String> datas) {
		String[] emails = new String[datas.size()];
		emails = datas.toArray(emails);

		final CharSequence[] items = emails;
		AlertDialog.Builder builder = new AlertDialog.Builder(act);
		builder.setTitle("Pilih Email");
		builder.setSingleChoiceItems(items, -1,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String email = items[which].toString();
						if (email.length() > 1) {
							// SharedPref.saveEmailPref(act, email);
						}
					}
				});
		builder.show();

	}

	public static String getAccountGoogleUser(Context context) {
		String res = "";

		Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
		Account[] accounts = AccountManager.get(context).getAccounts();
		for (Account account : accounts) {
			if (emailPattern.matcher(account.name).matches()) {
				String possibleEmail = account.name;
				res = possibleEmail;

				if (res.indexOf("gmail") > -1)
					break;
			}
		}

		if (accounts.length > 0 && res.length() < 1)
			res = accounts[0].name;

		log("get email address: " + res);

		return res;
	}

	public static String getPhoneNumberOwner(Context ctx) {

		TelephonyManager info = (TelephonyManager) ctx
				.getSystemService(Context.TELEPHONY_SERVICE);
		String phoneNumber = info.getLine1Number();
		if (phoneNumber == null)
			phoneNumber = "";

		return phoneNumber;
	}
	
	public static void setPolicyThread() {

		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		} catch (Exception e) {
		}
	}
	
	public static void toast(Context ctx, String text) {
		Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
	}

	public static void log(String text) {
		Log.e("Utils TAG", text);
	}
}
