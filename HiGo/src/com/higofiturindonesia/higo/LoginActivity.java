package com.higofiturindonesia.higo;

import com.higofiturindonesia.higo.page.CategoryFreePage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class LoginActivity extends Activity {

	ProgressBar progress;
	RelativeLayout layoutButton;
	Button signup, login, loginfb, continueasquest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		layoutButton = (RelativeLayout) findViewById(R.id.layoutButton);

		progress = (ProgressBar) findViewById(R.id.progress);
		progress.setVisibility(View.GONE);

		signup = (Button) findViewById(R.id.signup);
		login = (Button) findViewById(R.id.login);
		loginfb = (Button) findViewById(R.id.loginfb);
		continueasquest = (Button) findViewById(R.id.continueguest);
		
		continueasquest.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				gotoCategoryFree();
			}
		});

		signup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				gotoSignupActivity();
			}
		});

		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(LoginActivity.this,
						LoginPage.class), 1001);
			}
		});

		loginfb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// startActivity(new Intent(LoginActivity.this,
				// LoginPage.class));
			}
		});

		startAnimation();
	}

	public void startAnimation() {

		layoutButton.setVisibility(View.VISIBLE);
		Animation animation1 = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.fade);
		layoutButton.startAnimation(animation1);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// layoutTop.
				hideKeyboard();
			}
		}, 900);
	}

	private void hideKeyboard() {
		// Check if no view has focus:
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager inputManager = (InputMethodManager) this
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(view.getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	void gotoMainActivity() {

		Intent i = new Intent(LoginActivity.this, MainActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);

		// close this activity
		finish();
	}

	void gotoSignupActivity() {

		Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);

		// close this activity
		//finish();
	}
	
	void gotoCategoryFree() {

		Intent i = new Intent(LoginActivity.this, CategoryFreePage.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);

		// close this activity
		//finish();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1001) {
			if (resultCode == Activity.RESULT_OK) {
				finish();
			}
		}

	}
}
