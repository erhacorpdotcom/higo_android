package com.higofiturindonesia.higo.model;

import org.json.JSONObject;

public class VendorModel {

	String id;
	String kode; // id_vendor

	/*
	 * {"id_vendor":"1","nama_vendor":"Femina", "alamat":
	 * "jl. skip lama NO 21 RT 32 Kelurahan Teluk Dalam Kecamatan Banjarmasin Tengah Banjarmasin"
	 * , "no_telp":"+6287873506683","no_hp":"+6287873506683","email":
	 * "flyhigh3r@gmail.com",
	 * "fax":"70114","date_vendor":"2015-08-04 14:05:13","contact_person"
	 * :"Pak Rudy",
	 * "city_vendor":"Banjarmasin","id_category":"1","id_category2":
	 * "2","year":"2014,2015",
	 * "tipe_content":"0","tipe_vendor":"2","status":"1",
	 * "image_vendor":"upload\/51f0dd80c8823e93e59f6206d70debe6_image_vendor.png",
	 * "image_vendor_hover"
	 * :"upload\/51f0dd80c8823e93e59f6206d70debe6_image_vendor_hover.png",
	 * "real_logo_vendor"
	 * :"upload\/fd87f8510c9c6ea6a50edb3acd7bbd64_image_vendor_dashboard.png",
	 * "link_vendor"
	 * :"","web_vendor":"http:\/\/nexusmods.com\/skyrim\/mods\/30936\/?",
	 * "description"
	 * :"Femina","kode_pos":"70114","duration_subscriber":"2592000000"
	 * ,"total_edition":"4",
	 * "disc_total_edition":"10","total_for_disc":"1","price"
	 * :"12000","price_subscriber":"36000"}
	 */

	String name;
	String address;
	String phone;
	String mobile;
	String email;
	String fax;
	String date;
	String contactPerson;
	String city;
	String idCategory1;
	String idCategory2;

	String year;

	int tipeContent;
	int tipeVendor;
	int status;

	String image;
	String imageHover;
	String imageReal;

	String link;
	String web;

	String description;
	String kodePos;
	long duration;

	int totalEdition;
	int discEdition;
	int discTotal;

	double price;
	double priceSubscriber;

	int icon;
	boolean checked = false;
	boolean isShowNew = false;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getIdCategory1() {
		return idCategory1;
	}

	public void setIdCategory1(String idCategory1) {
		this.idCategory1 = idCategory1;
	}

	public String getIdCategory2() {
		return idCategory2;
	}

	public void setIdCategory2(String idCategory2) {
		this.idCategory2 = idCategory2;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public int getTipeContent() {
		return tipeContent;
	}

	public void setTipeContent(int tipeContent) {
		this.tipeContent = tipeContent;
	}

	public int getTipeVendor() {
		return tipeVendor;
	}

	public void setTipeVendor(int tipeVendor) {
		this.tipeVendor = tipeVendor;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImageHover() {
		return imageHover;
	}

	public void setImageHover(String imageHover) {
		this.imageHover = imageHover;
	}

	public String getImageReal() {
		return imageReal;
	}

	public void setImageReal(String imageReal) {
		this.imageReal = imageReal;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getWeb() {
		return web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKodePos() {
		return kodePos;
	}

	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public int getTotalEdition() {
		return totalEdition;
	}

	public void setTotalEdition(int totalEdition) {
		this.totalEdition = totalEdition;
	}

	public int getDiscEdition() {
		return discEdition;
	}

	public void setDiscEdition(int discEdition) {
		this.discEdition = discEdition;
	}

	public int getDiscTotal() {
		return discTotal;
	}

	public void setDiscTotal(int discTotal) {
		this.discTotal = discTotal;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getPriceSubscriber() {
		return priceSubscriber;
	}

	public void setPriceSubscriber(double priceSubscriber) {
		this.priceSubscriber = priceSubscriber;
	}
	
	public boolean isShowNew() {
		return isShowNew;
	}

	public void setShowNew(boolean isShowNew) {
		this.isShowNew = isShowNew;
	}

	public static VendorModel setVendorFromModelJSON(String data) {
		VendorModel v = new VendorModel();

		try {
			JSONObject json = new JSONObject(data);
			/*
			 * {"id_vendor":"1","nama_vendor":"Femina", "alamat":
			 * "jl. skip lama NO 21 RT 32 Kelurahan Teluk Dalam Kecamatan Banjarmasin Tengah Banjarmasin"
			 * , "no_telp":"+6287873506683","no_hp":"+6287873506683",
			 * "email":"flyhigh3r@gmail.com"
			 * ,"fax":"70114","date_vendor":"2015-08-04 14:05:13",
			 * "contact_person"
			 * :"Pak Rudy","city_vendor":"Banjarmasin","id_category":"1",
			 * "id_category2"
			 * :"2","year":"2014,2015","tipe_content":"0","tipe_vendor":"2",
			 * "status":"1","image_vendor":
			 * "upload\/51f0dd80c8823e93e59f6206d70debe6_image_vendor.png",
			 * "image_vendor_hover"
			 * :"upload\/51f0dd80c8823e93e59f6206d70debe6_image_vendor_hover.png",
			 * "real_logo_vendor":
			 * "upload\/fd87f8510c9c6ea6a50edb3acd7bbd64_image_vendor_dashboard.png",
			 * "link_vendor"
			 * :"","web_vendor":"http:\/\/nexusmods.com\/skyrim\/mods\/30936\/?",
			 * "description"
			 * :"Femina","kode_pos":"70114","duration_subscriber":"2592000000",
			 * "total_edition"
			 * :"4","disc_total_edition":"10","total_for_disc":"1",
			 * "price":"12000","price_subscriber":"36000"}
			 */

			v.setId("");
			v.setIcon(0);
			v.setChecked(false);

			v.setKode(json.getString("id_vendor"));
			v.setName(json.getString("nama_vendor"));

			v.setImage(json.getString("image_vendor"));
			v.setImageHover(json.getString("image_vendor_hover"));
			v.setImageReal("");

			try {
				if (json.getString("real_logo_vendor") != null)
					v.setImageReal(json.getString("real_logo_vendor"));
			} catch (Exception e) {
			}

			v.setAddress("");
			try {
				if (json.getString("alamat") != null)
					v.setAddress(json.getString("alamat"));
			} catch (Exception e) {
			}

			v.setPhone(json.getString("no_telp"));
			v.setMobile(json.getString("no_hp"));
			v.setEmail(json.getString("email"));
			v.setFax(json.getString("fax"));
			v.setDate(json.getString("date_vendor"));
			v.setContactPerson(json.getString("contact_person"));

			v.setCity(json.getString("city_vendor"));
			v.setIdCategory1(json.getString("id_category"));
			v.setIdCategory2(json.getString("id_category2"));
			v.setYear(json.getString("year"));

			v.setTipeContent(json.getInt("tipe_content"));
			v.setTipeVendor(json.getInt("tipe_vendor"));
			v.setStatus(json.getInt("status"));

			v.setLink(json.getString("link_vendor"));
			v.setWeb(json.getString("web_vendor"));
			v.setDescription(json.getString("description"));

			v.setKodePos(json.getString("kode_pos"));
			v.setDuration(json.getLong("duration_subscriber"));
			v.setTotalEdition(json.getInt("total_edition"));
			v.setDiscEdition(json.getInt("disc_total_edition"));
			v.setDiscTotal(json.getInt("total_for_disc"));

			v.setPrice(json.getDouble("price"));
			v.setPriceSubscriber(json.getDouble("price_subscriber"));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return v;
	}

}
