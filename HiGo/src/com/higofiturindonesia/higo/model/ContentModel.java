package com.higofiturindonesia.higo.model;

import org.json.JSONObject;

public class ContentModel {
	
	// http://higoapps.com/api/content_api.php?action=get_content_free&im=8
	
	String id;
	/*
	{"id_content":"118","nama_content":"Premiere Vol.1 #8",
		"id_category":"3","id_category2":"7","date_content":"2015-08-03 11:50:24",
		"web_content":"","id_vendor":"12","id_subvendor":"0",
		"image_1":"upload\/12_55918fb35495f75585618bf5a6182af5_image_content.png",
		"image_2":"upload\/12_55918fb35495f75585618bf5a6182af5_sneakpeek_content.png",
		"image_3":"","image_4":"","image_5":"",
		"data_file":"upload\/12_55918fb35495f75585618bf5a6182af5_file_content.pdf",
		"description":"Premiere Vol.1 #8","status":"2","update_content":"2015-08-03 11:50:24",
		"total_comment":"0","total_rating":"0","currency_content":"IDR","price_content":"20000",
		"disc_content":"10","tipe_content":"1",
		"access_token_key":"2876921466261685283475f5015ed56b1b9aac364bb324cd95bf143884e8a151",
		"link_content":""}
	*/
	
	String idContent;
	String name;
	String idCategory;
	String idCategory2;
	
	String dateCreated;
	String dateUpdated;
	String web;
	String idVendor;
	String idSubVendor;
	
	String image1;
	String image2;
	String image3;
	String image4;
	String image5;
	
	String dataFile;
	String description;
	int status;
	int totalComment;
	double totalRating;
	
	String currency;
	double price;
	int disc;
	int tipe;
	
	String accessToken;
	String link;
	String categoryName;
	boolean checked;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdContent() {
		return idContent;
	}
	public void setIdContent(String idContent) {
		this.idContent = idContent;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdCategory() {
		return idCategory;
	}
	public void setIdCategory(String idCategory) {
		this.idCategory = idCategory;
	}
	public String getIdCategory2() {
		return idCategory2;
	}
	public void setIdCategory2(String idCategory2) {
		this.idCategory2 = idCategory2;
	}
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(String dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	public String getWeb() {
		return web;
	}
	public void setWeb(String web) {
		this.web = web;
	}
	public String getIdVendor() {
		return idVendor;
	}
	public void setIdVendor(String idVendor) {
		this.idVendor = idVendor;
	}
	public String getIdSubVendor() {
		return idSubVendor;
	}
	public void setIdSubVendor(String idSubVendor) {
		this.idSubVendor = idSubVendor;
	}
	public String getImage1() {
		return image1;
	}
	public void setImage1(String image1) {
		this.image1 = image1;
	}
	public String getImage2() {
		return image2;
	}
	public void setImage2(String image2) {
		this.image2 = image2;
	}
	public String getImage3() {
		return image3;
	}
	public void setImage3(String image3) {
		this.image3 = image3;
	}
	public String getImage4() {
		return image4;
	}
	public void setImage4(String image4) {
		this.image4 = image4;
	}
	public String getImage5() {
		return image5;
	}
	public void setImage5(String image5) {
		this.image5 = image5;
	}
	public String getDataFile() {
		return dataFile;
	}
	public void setDataFile(String dataFile) {
		this.dataFile = dataFile;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getTotalComment() {
		return totalComment;
	}
	public void setTotalComment(int totalComment) {
		this.totalComment = totalComment;
	}
	public double getTotalRating() {
		return totalRating;
	}
	public void setTotalRating(double totalRating) {
		this.totalRating = totalRating;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getDisc() {
		return disc;
	}
	public void setDisc(int disc) {
		this.disc = disc;
	}
	public int getTipe() {
		return tipe;
	}
	public void setTipe(int tipe) {
		this.tipe = tipe;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	

	public static ContentModel setContentFromModelJSON(String data) {
		ContentModel v = new ContentModel();

		try {
			JSONObject json = new JSONObject(data);
			/*
			 {"id_content":"118","nama_content":"Premiere Vol.1 #8",
			"id_category":"3","id_category2":"7","date_content":"2015-08-03 11:50:24",
			"web_content":"","id_vendor":"12","id_subvendor":"0",
			"image_1":"upload\/12_55918fb35495f75585618bf5a6182af5_image_content.png",
			"image_2":"upload\/12_55918fb35495f75585618bf5a6182af5_sneakpeek_content.png",
			"image_3":"","image_4":"","image_5":"",
			"data_file":"upload\/12_55918fb35495f75585618bf5a6182af5_file_content.pdf",
			"description":"Premiere Vol.1 #8","status":"2","update_content":"2015-08-03 11:50:24",
			"total_comment":"0","total_rating":"0","currency_content":"IDR","price_content":"20000",
			"disc_content":"10","tipe_content":"1",
			"access_token_key":"2876921466261685283475f5015ed56b1b9aac364bb324cd95bf143884e8a151",
			"link_content":""}
			 */

			v.setId("");
			v.setIdContent(json.getString("id_content"));
			v.setName(json.getString("nama_content"));
			v.setIdCategory(json.getString("id_category"));
			v.setIdCategory2(json.getString("id_category2"));
			v.setDateCreated(json.getString("date_content"));
			v.setDateUpdated(json.getString("update_content"));
			
			v.setIdVendor(json.getString("id_vendor"));
			v.setIdSubVendor(json.getString("id_subvendor"));
			v.setImage1(json.getString("image_1"));
			v.setImage2(json.getString("image_2"));
			v.setImage3(json.getString("image_3"));
			v.setImage4(json.getString("image_4"));
			v.setImage5(json.getString("image_5"));
			
			v.setDataFile(json.getString("data_file"));
			v.setDescription(json.getString("description"));
			v.setStatus(json.getInt("status"));
			v.setTotalComment(json.getInt("total_comment"));
			v.setTotalRating(json.getDouble("total_rating"));
			
			v.setCurrency(json.getString("currency_content"));
			v.setPrice(json.getDouble("price_content"));
			v.setDisc(json.getInt("disc_content"));
			v.setTipe(json.getInt("tipe_content"));
			v.setAccessToken(json.getString("access_token_key"));
			
			v.setWeb(json.getString("web_content"));
			v.setLink(json.getString("link_content"));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return v;
	}
}
