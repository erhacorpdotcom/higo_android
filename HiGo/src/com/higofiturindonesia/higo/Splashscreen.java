package com.higofiturindonesia.higo;

import com.higofiturindonesia.higo.page.ChooseCategoryPage;
import com.higofiturindonesia.higo.util.SharedPref;
import com.higofiturindonesia.higo.util.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splashscreen extends Activity {

	private static int SPLASH_TIME_OUT = 3500;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splashscreen);

		// fake autologout
		//SharedPref.clearAllPreferenceRelated(this);
		
		Utils.log("get status pref: "+SharedPref.getStatusPref(Splashscreen.this));
		
		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity
				Intent i = new Intent(Splashscreen.this, LoginActivity.class);
				
				if (SharedPref.getStatusPref(Splashscreen.this).equals("1")) 
					i = new Intent(Splashscreen.this, MainActivity.class);
				
				if (SharedPref.getStatusPref(Splashscreen.this).equals("2")) 
					i = new Intent(Splashscreen.this, ChooseCategoryPage.class);
				
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);

				// close this activity
				finish();
			}
		}, SPLASH_TIME_OUT);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
