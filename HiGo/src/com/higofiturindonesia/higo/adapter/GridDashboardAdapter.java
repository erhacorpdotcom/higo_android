package com.higofiturindonesia.higo.adapter;

import java.util.ArrayList;

import com.higofiturindonesia.higo.R;
import com.higofiturindonesia.higo.model.VendorModel;
import com.higofiturindonesia.higo.util.Constant;
import com.higofiturindonesia.higo.util.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class GridDashboardAdapter extends BaseAdapter {

	ArrayList<VendorModel> models = new ArrayList<VendorModel>();
	Activity act;
	LayoutInflater inflater;
	ImageLoader imgLoader;
	boolean isDashboard;
	
	public GridDashboardAdapter(Activity act, ArrayList<VendorModel> models, 
			boolean isDashboard) {
		this.act = act;
		this.models = models;
		this.isDashboard = isDashboard;
		this.inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgLoader = new ImageLoader(act);
	}
	
	public void setItemData(ArrayList<VendorModel> models) {
		this.models = models;
		notifyDataSetChanged();
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return models.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return models.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder {
		ImageView image, newImage;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		final ViewHolder holder;

		if (view == null) {
			view = inflater.inflate(R.layout.item_grid_dashboard, parent, false);

			holder = new ViewHolder();

			holder.image = (ImageView) view.findViewById(R.id.image);
			holder.newImage = (ImageView) view.findViewById(R.id.newIcon);
			
			view.setTag(holder);
			
		} else {
			holder = (ViewHolder) view.getTag();
		}

		try {

			VendorModel model = models.get(position);
			String uri = "";
			if (model.getImage()!=null)
				uri = model.getImage();
			
			//Utils.log("get image uri: "+uri);
			// image for new icon show
			holder.newImage.setVisibility(View.GONE);
			if (model.isShowNew()) {
				holder.newImage.setVisibility(View.VISIBLE);
			}
			
			if (model.getIcon()>0) {
				holder.image.setBackgroundResource(model.getIcon());
			}
			
			if (uri!=null && uri.length()>0) {
				if (this.isDashboard) {
					uri = Constant.URI_BASE+""+model.getImageReal();
				}
				else if (model.isChecked()) 
					uri = Constant.URI_BASE+""+model.getImageHover();
				else
					uri = Constant.URI_BASE+""+uri;
				
				//Utils.log("get image uri: "+uri);
				imgLoader.DisplayImage(uri, R.drawable.circle_empty, holder.image);
			}
			
			
		} catch (Exception ee) {
			ee.printStackTrace();
		}
		
		return view;
	}
	
	public ArrayList<VendorModel> getModels() {
		return this.models;
	}
	
	public int countCheckedItem() {
		int count = 0;
		for (int k=0; k<models.size(); k++) {
			VendorModel v = models.get(k);
			if (v.isChecked())
				count++;
		}
		
		if (count>0)
			return count;
		
		return 0;
	}

	public void toggleChecked(int position) {
		ArrayList<VendorModel> temps = new ArrayList<VendorModel>();
		for (int i=0; i<models.size(); i++) {
			VendorModel m = models.get(i);
			if (position == i) {
				m.setChecked(!m.isChecked());
			}
			
			temps.add(m);
		}
		
		if (temps.size()>0) {
			this.models = temps;
			notifyDataSetChanged();
		}
	}
	
	
}
