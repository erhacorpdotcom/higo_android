package com.higofiturindonesia.higo.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.higofiturindonesia.higo.R;
import com.higofiturindonesia.higo.model.ContentModel;
import com.higofiturindonesia.higo.util.Constant;
import com.higofiturindonesia.higo.util.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ContentFreeAdapter extends BaseAdapter {

	ArrayList<ContentModel> models = new ArrayList<ContentModel>();
	Activity act;
	LayoutInflater inflater;
	ImageLoader imgLoader;

	public ContentFreeAdapter(Activity act, ArrayList<ContentModel> models) {
		this.act = act;
		this.models = models;
		this.inflater = (LayoutInflater) act
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgLoader = new ImageLoader(act);
	}

	public void setItemData(ArrayList<ContentModel> models) {
		this.models = models;
		notifyDataSetChanged();

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return models.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return models.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder {
		ImageView image;
		TextView title, monthYear, categ;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		final ViewHolder holder;

		if (view == null) {
			view = inflater.inflate(R.layout.item_content_free, parent, false);

			holder = new ViewHolder();

			holder.image = (ImageView) view.findViewById(R.id.image);
			holder.title = (TextView) view.findViewById(R.id.title);
			holder.monthYear = (TextView) view.findViewById(R.id.monthYear);
			holder.categ = (TextView) view.findViewById(R.id.categ);

			view.setTag(holder);

		} else {
			holder = (ViewHolder) view.getTag();
		}

		try {

			ContentModel model = models.get(position);
			holder.title.setText("" + model.getName());

			try {
				SimpleDateFormat format = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				Date date = format.parse(model.getDateCreated());
				// String dayOfTheWeek = (String)
				// android.text.format.DateFormat.format("EEEE",
				// date);//Thursday
				String stringMonth = (String) android.text.format.DateFormat
						.format("MMMM", date); // Jun
				// String intMonth = (String)
				// android.text.format.DateFormat.format("MM", date); //06
				String year = (String) android.text.format.DateFormat.format(
						"yyyy", date); // 2013
				// String day = (String)
				// android.text.format.DateFormat.format("dd", date); //20

				holder.monthYear.setText("" + stringMonth + " " + year);
			} catch (Exception e) {
			}

			try {
				int categ = Integer.parseInt(model.getIdCategory());
				String categName = Constant.CATEG_NAME[categ - 1];
				holder.categ.setText("" + categName);
			} catch (Exception e) {
			}

			String uri = "";
			if (model.getImage1() != null)
				uri = model.getImage1();

			if (uri != null && uri.length() > 0) {
				uri = Constant.URI_BASE + "" + uri;

				//Utils.log("get image uri: "+uri);
				imgLoader.DisplayImage(uri, R.drawable.loading_square,
						holder.image);
			}

		} catch (Exception ee) {
			ee.printStackTrace();
		}

		return view;
	}

	public ArrayList<ContentModel> getModels() {
		return this.models;
	}

	public int countCheckedItem() {
		int count = 0;
		for (int k = 0; k < models.size(); k++) {
			ContentModel v = models.get(k);
			if (v.isChecked())
				count++;
		}

		if (count > 0)
			return count;

		return 0;
	}

	public void toggleChecked(int position) {
		ArrayList<ContentModel> temps = new ArrayList<ContentModel>();
		for (int i = 0; i < models.size(); i++) {
			ContentModel m = models.get(i);
			if (position == i) {
				m.setChecked(!m.isChecked());
			}

			temps.add(m);
		}

		if (temps.size() > 0) {
			this.models = temps;
			notifyDataSetChanged();
		}
	}

}
