package com.higofiturindonesia.higo.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.higofiturindonesia.higo.R;

public class LeftMenuAdapter extends BaseAdapter {

	int[] datas = new int[]{R.drawable.freemenu, R.drawable.categoriesmenu, R.drawable.settingmenu};
	
	Activity act;
	LayoutInflater inflater;
	
	public LeftMenuAdapter(Activity act) {
		this.act = act;
		this.inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return datas.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return datas[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder {
		ImageView image;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		final ViewHolder holder;

		if (view == null) {
			view = inflater.inflate(R.layout.item_left_menu, parent, false);

			holder = new ViewHolder();
			holder.image = (ImageView) view.findViewById(R.id.image);
			
			view.setTag(holder);
			
		} else {
			holder = (ViewHolder) view.getTag();
		}

		try {

			holder.image.setImageResource(datas[position]);
			
			
		} catch (Exception ee) {
			ee.printStackTrace();
		}
		
		return view;
	}
	
}
