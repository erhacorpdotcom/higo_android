package com.higofiturindonesia.higo.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.higofiturindonesia.higo.R;
import com.higofiturindonesia.higo.util.Constant;
import com.higofiturindonesia.higo.util.Utils;

public class GridCategoryAdapter extends BaseAdapter {

	int[] datas = new int[]{R.drawable.categ02, R.drawable.categ03, R.drawable.categ04, 
			R.drawable.categ05, R.drawable.categ06, R.drawable.categ07,
			R.drawable.categ08, R.drawable.categ09};
	
	String[] titles = Constant.CATEG_NAME;
	
	boolean[] checked;
	
	Activity act;
	LayoutInflater inflater;
	
	public GridCategoryAdapter(Activity act, boolean[] checked) {
		this.act = act;
		this.checked = checked;
		this.inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public boolean[] getChecked() {
		return this.checked;
	}
	
	public void setChecked(int position) {
		Utils.log("setChecked: "+position);
		
		boolean[] temp = new boolean[checked.length];
		for (int j=0; j<checked.length; j++) {
			boolean t = checked[j];
			if (j==position) {
				t = !t;
			}
			
			Utils.log("setChecked looping: "+t);
			temp[j] = t;
		}
		
		this.checked = temp;
		notifyDataSetChanged();
	}
	
	public int countCheckedItem() {
		int count = 0;
		for (int k=0; k<checked.length; k++) {
			if (checked[k])
				count++;
		}
		
		if (count>0)
			return count;
		
		return 0;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return datas.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return datas[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder {
		ImageView image;
		TextView text;
		LinearLayout layoutCateg;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		final ViewHolder holder;
		
		if (view == null) {
			view = inflater.inflate(R.layout.item_category, parent, false);

			holder = new ViewHolder();
			holder.image = (ImageView) view.findViewById(R.id.image);
			holder.text = (TextView) view.findViewById(R.id.text);
			holder.layoutCateg = (LinearLayout) view.findViewById(R.id.layoutCateg);
			
			view.setTag(holder);
			
		} else {
			holder = (ViewHolder) view.getTag();
			
		}

		try {
			boolean check = this.checked[position];
			
			holder.layoutCateg.setBackgroundResource(R.color.transparant);
			if (check) {
				holder.layoutCateg.setBackgroundResource(R.color.orange_transparant);
			}

			holder.image.setBackgroundResource(datas[position]);
			holder.text.setText(""+titles[position]);
			
		} catch (Exception ee) {
			ee.printStackTrace();
		}
		
		return view;
	}
	
}
